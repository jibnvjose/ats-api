﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationTrackingSystemAPI.Controllers
{
    [Route("api/job")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly IDataRepository<JobDescription, JobDescriptionDTO> _dataRepository;

        public JobController(IDataRepository<JobDescription, JobDescriptionDTO> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var jobs = _dataRepository.GetAll();
            return Ok(jobs);
        }

        // GET: api/jobs/5
        [HttpGet("{id}", Name = "GetJob")]
        public IActionResult Get(int id)
        {
            var job = _dataRepository.GetDto(id);
            //var job = _dataRepository.Get(id);
            if (job == null)
            {
                return NotFound("Job not found.");
            }
            return Ok(job);
        }

        // POST: api/jobs
        [HttpPost]
        public IActionResult Post([FromBody] JobDescription jobDescription)
        {
            if (jobDescription is null)
            {
                return BadRequest("JobDescription is null.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _dataRepository.Add(jobDescription);
            return CreatedAtRoute("GetJob", new { Id = jobDescription.JobId }, null);
        }

        // PUT: api/job/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] JobDescription jobDescription)
        {
            if (jobDescription == null)
            {
                return BadRequest("JobDescription is null.");
            }
            var jobDescriptionToUpdate = _dataRepository.Get(id);
            if (jobDescriptionToUpdate == null)
            {
                return NotFound("The JobDescription record couldn't be found.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _dataRepository.Update(jobDescriptionToUpdate, jobDescription);
            return NoContent();
        }
    }
}