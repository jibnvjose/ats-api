﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationTrackingSystemAPI.Controllers
{
    [Route("api/skillSets")]
    [ApiController]
    public class SkillSetController : ControllerBase
    {
        private readonly IDataRepository<SkillSet, SkillSetDTO> _dataRepository;

        public SkillSetController(IDataRepository<SkillSet, SkillSetDTO> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var skillSets = _dataRepository.GetAll();
            return Ok(skillSets);
        }
    }
}