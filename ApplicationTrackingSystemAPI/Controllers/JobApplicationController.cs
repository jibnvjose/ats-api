﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationTrackingSystemAPI.Controllers
{
    [Route("api/jobApplications")]
    [ApiController]
    public class JobApplicationController : ControllerBase
    {
        private readonly IDataRepository<JobApplication, JobApplicationDTO> _dataRepository;
        public JobApplicationController(IDataRepository<JobApplication, JobApplicationDTO> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var jobApplications = _dataRepository.GetAll();
            return Ok(jobApplications);
        }
    }
}