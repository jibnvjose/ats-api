﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationTrackingSystemAPI.Controllers
{
    [Route("api/expertiseLevels")]
    [ApiController]
    public class ExpertiseLevelLookupController : ControllerBase
    {
        private readonly IDataRepository<ExpertiseLevelLookup, ExpertiseLevelLookupDTO> _dataRepository;

        public ExpertiseLevelLookupController(IDataRepository<ExpertiseLevelLookup, ExpertiseLevelLookupDTO> dataRepository)
        {
            _dataRepository = dataRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var expertiseLevels = _dataRepository.GetAll();
            return Ok(expertiseLevels);
        }
    }
}