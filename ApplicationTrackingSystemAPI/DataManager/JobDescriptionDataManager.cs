﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DataManager
{
    public class JobDescriptionDataManager: IDataRepository<JobDescription, JobDescriptionDTO>
    {
        private ApplicationTrackingSystemContext _applicationTrackingSystemContext;
        public JobDescriptionDataManager(ApplicationTrackingSystemContext applicationTrackingSystemContext)
        {
            _applicationTrackingSystemContext = applicationTrackingSystemContext;
        }
        public IEnumerable<JobDescription> GetAll()
        {
            return _applicationTrackingSystemContext.JobDescription
                .Include(jobDescription=> jobDescription.JobApplication)
                .ToList();
        }
        public JobDescription Get(long id)
        {
            _applicationTrackingSystemContext.ChangeTracker.LazyLoadingEnabled = false;
            var jobDescription = _applicationTrackingSystemContext.JobDescription
                  .SingleOrDefault(j => j.JobId == id);
            if (jobDescription == null)
            {
                return null;
            }
            _applicationTrackingSystemContext.Entry(jobDescription)
                .Collection(j => j.JobApplication)
                .Load();
            _applicationTrackingSystemContext.Entry(jobDescription)
                .Collection(j => j.JobSkillWeightageRequired)
                .Load();

            return jobDescription;
        }
        public JobDescriptionDTO GetDto(long id)
        {
            _applicationTrackingSystemContext.ChangeTracker.LazyLoadingEnabled = true;
            using (var context = new ApplicationTrackingSystemContext())
            {
                var jobDescription = context.JobDescription
                    .Include(j=>j.JobSkillWeightageRequired)
                    .ThenInclude(e=> e.Skill)
                    .SingleOrDefault(j => j.JobId == id);
                return JobDescriptionDtoMapper.MapToDto(jobDescription);
            }
        }

        public void Add(JobDescription entity)
        {
            _applicationTrackingSystemContext.JobDescription.Add(entity);
            _applicationTrackingSystemContext.SaveChanges();
        }

        public void Update(JobDescription entityToUpdate, JobDescription entity)
        {
            entityToUpdate = _applicationTrackingSystemContext.JobDescription
                .Single(j => j.JobId == entityToUpdate.JobId);
            entityToUpdate.JobTitle = entity.JobTitle;
            entityToUpdate.JobCode = entity.JobCode;
            entityToUpdate.Description = entity.Description;
            entityToUpdate.CreatedDate = entity.CreatedDate;
            entityToUpdate.ExpiryDate = entity.ExpiryDate;
            entityToUpdate.Status = entity.Status;

            _applicationTrackingSystemContext.SaveChanges();
        }
        public static class JobDescriptionDtoMapper
        {
            public static JobDescriptionDTO MapToDto(JobDescription jobDescription)
            {
                return new JobDescriptionDTO()
                {
                    JobId = jobDescription.JobId,
                    JobTitle = jobDescription.JobTitle,
                    JobCode = jobDescription.JobCode,
                    Description = jobDescription.Description,
                    CreatedDate = jobDescription.CreatedDate,
                    ExpiryDate = jobDescription.ExpiryDate,
                    Status = jobDescription.Status,
                    JobSkillWeightageRequired = jobDescription.JobSkillWeightageRequired                };
            }
        }
        public void Delete(JobDescription entity)
        {
            _applicationTrackingSystemContext.Remove(entity);
            _applicationTrackingSystemContext.SaveChanges();
        }
    }
}
