﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DataManager
{
    public class SkillSetDataManager : IDataRepository<SkillSet, SkillSetDTO>
    {
        private ApplicationTrackingSystemContext _applicationTrackingSystemContext;
        public SkillSetDataManager(ApplicationTrackingSystemContext applicationTrackingSystemContext)
        {
            _applicationTrackingSystemContext = applicationTrackingSystemContext;
        }
        public IEnumerable<SkillSet> GetAll()
        {
            return _applicationTrackingSystemContext.SkillSet
                .ToList();
        }
        public SkillSet Get(long id)
        {
            throw new NotImplementedException();
        }
        public SkillSetDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(SkillSet entity)
        {
            throw new NotImplementedException();
        }

        public void Update(SkillSet entityToUpdate, SkillSet entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(SkillSet entity)
        {
            throw new NotImplementedException();
        }
    }
}
