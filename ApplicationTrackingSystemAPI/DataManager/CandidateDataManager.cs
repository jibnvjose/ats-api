﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DataManager
{
    public class CandidateDataManager : IDataRepository<Candidate, CandidateDTO>
    {
        private ApplicationTrackingSystemContext _applicationTrackingSystemContext;
        public CandidateDataManager(ApplicationTrackingSystemContext applicationTrackingSystemContext)
        {
            _applicationTrackingSystemContext = applicationTrackingSystemContext;
        }
        public IEnumerable<Candidate> GetAll()
        {
            return _applicationTrackingSystemContext.Candidate
                .Include(candidate=>candidate.JobApplication)
                .ToList();
        }
        public Candidate Get(long id)
        {
            throw new NotImplementedException();
        }
        public CandidateDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(Candidate entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Candidate entityToUpdate, Candidate entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(Candidate entity)
        {
            throw new NotImplementedException();
        }
    }
}
