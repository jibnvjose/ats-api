﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DataManager
{
    public class ExpertiseLevelLookupDataManager: IDataRepository<ExpertiseLevelLookup, ExpertiseLevelLookupDTO>
    {
        private ApplicationTrackingSystemContext _applicationTrackingSystemContext;
        public ExpertiseLevelLookupDataManager(ApplicationTrackingSystemContext applicationTrackingSystemContext)
        {
            _applicationTrackingSystemContext = applicationTrackingSystemContext;
        }
        public IEnumerable<ExpertiseLevelLookup> GetAll()
        {
            return _applicationTrackingSystemContext.ExpertiseLevelLookup
                .ToList();
        }
        public ExpertiseLevelLookup Get(long id)
        {
            throw new NotImplementedException();
        }
        public ExpertiseLevelLookupDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(ExpertiseLevelLookup entity)
        {
            throw new NotImplementedException();
        }

        public void Update(ExpertiseLevelLookup entityToUpdate, ExpertiseLevelLookup entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(ExpertiseLevelLookup entity)
        {
            throw new NotImplementedException();
        }
    }
}
