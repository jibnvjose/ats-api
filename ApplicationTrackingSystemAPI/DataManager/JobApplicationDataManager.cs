﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using ApplicationTrackingSystemAPI.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DataManager
{
    public class JobApplicationDataManager : IDataRepository<JobApplication, JobApplicationDTO>
    {
        private ApplicationTrackingSystemContext _applicationTrackingSystemContext;
        public JobApplicationDataManager(ApplicationTrackingSystemContext applicationTrackingSystemContext)
        {
            _applicationTrackingSystemContext = applicationTrackingSystemContext;
        }
        public IEnumerable<JobApplication> GetAll()
        {
            return _applicationTrackingSystemContext.JobApplication
                .Include(jobApplication=> jobApplication.Candidate)
                .Include(jobApplication=> jobApplication.Job)
                .ToList();
        }
        public JobApplication Get(long id)
        {
            throw new NotImplementedException();
        }
        public JobApplicationDTO GetDto(long id)
        {
            throw new NotImplementedException();
        }

        public void Add(JobApplication entity)
        {
            throw new NotImplementedException();
        }

        public void Update(JobApplication entityToUpdate, JobApplication entity)
        {
            throw new NotImplementedException();
        }
        public void Delete(JobApplication entity)
        {
            throw new NotImplementedException();
        }
    }
}
