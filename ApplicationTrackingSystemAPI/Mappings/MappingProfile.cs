﻿using ApplicationTrackingSystemAPI.DTO;
using ApplicationTrackingSystemAPI.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<JobSkillWeightageRequired, JobSkillWeightageRequiredDTO>();
            CreateMap<JobDescription, JobDescriptionDTO>();
        }
    }
}
