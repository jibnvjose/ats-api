﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class SkillSet
    {
        public SkillSet()
        {
            JobSkillWeightageApplied = new HashSet<JobSkillWeightageApplied>();
            JobSkillWeightageRequired = new HashSet<JobSkillWeightageRequired>();
        }

        public int SkillId { get; set; }
        public string Description { get; set; }
        public int? IsEnabled { get; set; }

        public virtual ICollection<JobSkillWeightageApplied> JobSkillWeightageApplied { get; set; }
        public virtual ICollection<JobSkillWeightageRequired> JobSkillWeightageRequired { get; set; }
    }
}
