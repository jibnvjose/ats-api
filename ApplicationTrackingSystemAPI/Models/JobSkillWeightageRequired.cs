﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class JobSkillWeightageRequired
    {
        public int JobSkillWeightageRequiredId { get; set; }
        public int JobId { get; set; }
        public int SkillId { get; set; }
        public int? ExpertiseLevelId { get; set; }
        public double? MinimumYearsOfExperienceRequired { get; set; }
        public double? MaximumYearsOfExperienceRequired { get; set; }
        public string IsMandatory { get; set; }
        public double? Weightage { get; set; }

        public virtual ExpertiseLevelLookup ExpertiseLevel { get; set; }
        public virtual JobDescription Job { get; set; }
        public virtual SkillSet Skill { get; set; }
    }
}
