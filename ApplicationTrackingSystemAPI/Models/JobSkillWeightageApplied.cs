﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class JobSkillWeightageApplied
    {
        public int JobSkillWeightageAppliedId { get; set; }
        public int? ApplicationId { get; set; }
        public int? SkillId { get; set; }
        public int? ExpertiseLevelId { get; set; }

        public virtual JobApplication Application { get; set; }
        public virtual ExpertiseLevelLookup ExpertiseLevel { get; set; }
        public virtual SkillSet Skill { get; set; }
    }
}
