﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class ApplicationTrackingSystemContext : DbContext
    {
        public ApplicationTrackingSystemContext()
        {
        }

        public ApplicationTrackingSystemContext(DbContextOptions<ApplicationTrackingSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Candidate> Candidate { get; set; }
        public virtual DbSet<ExpertiseLevelLookup> ExpertiseLevelLookup { get; set; }
        public virtual DbSet<JobApplication> JobApplication { get; set; }
        public virtual DbSet<JobDescription> JobDescription { get; set; }
        public virtual DbSet<JobSkillWeightageApplied> JobSkillWeightageApplied { get; set; }
        public virtual DbSet<JobSkillWeightageRequired> JobSkillWeightageRequired { get; set; }
        public virtual DbSet<SkillSet> SkillSet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=LAPTOP-DPN7308F;Database=ApplicationTrackingSystem;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Candidate>(entity =>
            {
                entity.ToTable("candidate");

                entity.Property(e => e.CandidateId).HasColumnName("candidate_id");

                entity.Property(e => e.CurrentEmployer)
                    .HasColumnName("current_employer")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfBirth)
                    .HasColumnName("date_of_birth")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmploymentStatus).HasColumnName("employment_status");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("first_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasColumnName("middle_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TotalYearsOfExperience).HasColumnName("total_years_of_experience");
            });

            modelBuilder.Entity<ExpertiseLevelLookup>(entity =>
            {
                entity.HasKey(e => e.ExpertiseLevelId);

                entity.ToTable("expertise_level_lookup");

                entity.Property(e => e.ExpertiseLevelId).HasColumnName("expertise_level_id");

                entity.Property(e => e.ExpertiseLevel)
                    .HasColumnName("expertise_level")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Score).HasColumnName("score");
            });

            modelBuilder.Entity<JobApplication>(entity =>
            {
                entity.HasKey(e => e.ApplicationId);

                entity.ToTable("job_application");

                entity.Property(e => e.ApplicationId).HasColumnName("application_id");

                entity.Property(e => e.CandidateId).HasColumnName("candidate_id");

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.Resume)
                    .HasColumnName("resume")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.JobApplication)
                    .HasForeignKey(d => d.CandidateId)
                    .HasConstraintName("FK_job_application_candidate");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.JobApplication)
                    .HasForeignKey(d => d.JobId)
                    .HasConstraintName("FK_job_application_job_description");
            });

            modelBuilder.Entity<JobDescription>(entity =>
            {
                entity.HasKey(e => e.JobId);

                entity.ToTable("job_description");

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpiryDate)
                    .HasColumnName("expiry_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.JobCode)
                    .HasColumnName("job_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("job_description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasColumnName("job_title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<JobSkillWeightageApplied>(entity =>
            {
                entity.ToTable("job_skill_weightage_applied");

                entity.Property(e => e.JobSkillWeightageAppliedId).HasColumnName("job_skill_weightage_applied_id");

                entity.Property(e => e.ApplicationId).HasColumnName("application_id");

                entity.Property(e => e.ExpertiseLevelId).HasColumnName("expertise_level_id");

                entity.Property(e => e.SkillId).HasColumnName("skill_id");

                entity.HasOne(d => d.Application)
                    .WithMany(p => p.JobSkillWeightageApplied)
                    .HasForeignKey(d => d.ApplicationId)
                    .HasConstraintName("FK_job_skill_weightage_applied_job_application");

                entity.HasOne(d => d.ExpertiseLevel)
                    .WithMany(p => p.JobSkillWeightageApplied)
                    .HasForeignKey(d => d.ExpertiseLevelId)
                    .HasConstraintName("FK_job_skill_weightage_applied_expertise_level_lookup");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.JobSkillWeightageApplied)
                    .HasForeignKey(d => d.SkillId)
                    .HasConstraintName("FK_job_skill_weightage_applied_skill_set");
            });

            modelBuilder.Entity<JobSkillWeightageRequired>(entity =>
            {
                entity.ToTable("job_skill_weightage_required");

                entity.Property(e => e.JobSkillWeightageRequiredId).HasColumnName("job_skill_weightage_required_id");

                entity.Property(e => e.ExpertiseLevelId).HasColumnName("expertise_level_id");

                entity.Property(e => e.IsMandatory)
                    .HasColumnName("is_mandatory")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.JobId).HasColumnName("job_id");

                entity.Property(e => e.MaximumYearsOfExperienceRequired).HasColumnName("maximum_years_of_experience_required");

                entity.Property(e => e.MinimumYearsOfExperienceRequired).HasColumnName("minimum_years_of_experience_required");

                entity.Property(e => e.SkillId).HasColumnName("skill_id");

                entity.Property(e => e.Weightage).HasColumnName("weightage");

                entity.HasOne(d => d.ExpertiseLevel)
                    .WithMany(p => p.JobSkillWeightageRequired)
                    .HasForeignKey(d => d.ExpertiseLevelId)
                    .HasConstraintName("FK_job_skill_weightage_required_expertise_level_lookup");

                entity.HasOne(d => d.Job)
                    .WithMany(p => p.JobSkillWeightageRequired)
                    .HasForeignKey(d => d.JobId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_job_skill_weightage_required_job_description");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.JobSkillWeightageRequired)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_job_skill_weightage_required_skill_set");
            });

            modelBuilder.Entity<SkillSet>(entity =>
            {
                entity.HasKey(e => e.SkillId);

                entity.ToTable("skill_set");

                entity.Property(e => e.SkillId).HasColumnName("skill_id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsEnabled).HasColumnName("is_enabled");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
