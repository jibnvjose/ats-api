﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class JobApplication
    {
        public JobApplication()
        {
            JobSkillWeightageApplied = new HashSet<JobSkillWeightageApplied>();
        }

        public int ApplicationId { get; set; }
        public int? JobId { get; set; }
        public int? CandidateId { get; set; }
        public string Resume { get; set; }
        public double? Rating { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual JobDescription Job { get; set; }
        public virtual ICollection<JobSkillWeightageApplied> JobSkillWeightageApplied { get; set; }
    }
}
