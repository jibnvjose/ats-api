﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class ExpertiseLevelLookup
    {
        public ExpertiseLevelLookup()
        {
            JobSkillWeightageApplied = new HashSet<JobSkillWeightageApplied>();
            JobSkillWeightageRequired = new HashSet<JobSkillWeightageRequired>();
        }

        public int ExpertiseLevelId { get; set; }
        public string ExpertiseLevel { get; set; }
        public int? Score { get; set; }

        public virtual ICollection<JobSkillWeightageApplied> JobSkillWeightageApplied { get; set; }
        public virtual ICollection<JobSkillWeightageRequired> JobSkillWeightageRequired { get; set; }
    }
}
