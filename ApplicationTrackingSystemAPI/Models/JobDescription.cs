﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class JobDescription
    {
        public JobDescription()
        {
            JobApplication = new HashSet<JobApplication>();
            JobSkillWeightageRequired = new HashSet<JobSkillWeightageRequired>();
        }

        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public string JobCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Status { get; set; }

        public virtual ICollection<JobApplication> JobApplication { get; set; }
        public virtual ICollection<JobSkillWeightageRequired> JobSkillWeightageRequired { get; set; }
    }
}
