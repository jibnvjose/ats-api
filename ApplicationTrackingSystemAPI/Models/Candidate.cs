﻿using System;
using System.Collections.Generic;

namespace ApplicationTrackingSystemAPI.Models
{
    public partial class Candidate
    {
        public Candidate()
        {
            JobApplication = new HashSet<JobApplication>();
        }

        public int CandidateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double TotalYearsOfExperience { get; set; }
        public int EmploymentStatus { get; set; }
        public string CurrentEmployer { get; set; }

        public virtual ICollection<JobApplication> JobApplication { get; set; }
    }
}
