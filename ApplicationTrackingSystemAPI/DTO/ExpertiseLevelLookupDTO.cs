﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class ExpertiseLevelLookupDTO
    {
        public int ExpertiseLevelId { get; set; }
        public string ExpertiseLevel { get; set; }
        public int? Score { get; set; }
    }
}
