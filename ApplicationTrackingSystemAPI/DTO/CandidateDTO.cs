﻿using ApplicationTrackingSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class CandidateDTO
    {
        public int CandidateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public double TotalYearsOfExperience { get; set; }
        public int EmploymentStatus { get; set; }
        public string CurrentEmployer { get; set; }

        public virtual ICollection<JobApplication> JobApplication { get; set; }
    }
}
