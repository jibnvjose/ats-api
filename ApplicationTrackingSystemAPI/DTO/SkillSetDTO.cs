﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class SkillSetDTO
    {
        public int SkillId { get; set; }
        public string Description { get; set; }
        public int? IsEnabled { get; set; }
    }
}
