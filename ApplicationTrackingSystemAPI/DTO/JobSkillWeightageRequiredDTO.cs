﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class JobSkillWeightageRequiredDTO
    {
        public int JobSkillWeightageRequiredId { get; set; }
        public int JobId { get; set; }
        public int SkillId { get; set; }
        public int? ExpertiseLevelId { get; set; }
        public double? MinimumYearsOfExperienceRequired { get; set; }
        public double? MaximumYearsOfExperienceRequired { get; set; }
        public string IsMandatory { get; set; }
        public double? Weightage { get; set; }
        public virtual SkillSetDTO Skill { get; set; }
    }
}
