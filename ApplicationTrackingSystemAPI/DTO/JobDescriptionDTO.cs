﻿using ApplicationTrackingSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class JobDescriptionDTO
    {
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public string JobCode { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Status { get; set; }

        public virtual ICollection<JobApplication> JobApplication { get; set; }
        public virtual ICollection<JobSkillWeightageRequired> JobSkillWeightageRequired { get; set; }
    }
}
