﻿using ApplicationTrackingSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationTrackingSystemAPI.DTO
{
    public class JobApplicationDTO
    {
        public int ApplicationId { get; set; }
        public int? JobId { get; set; }
        public int? CandidateId { get; set; }
        public string Resume { get; set; }
        public double? Rating { get; set; }

        public virtual Candidate Candidate { get; set; }
        public virtual JobDescription Job { get; set; }
        public virtual ICollection<JobSkillWeightageApplied> JobSkillWeightageApplied { get; set; }
    }
}
